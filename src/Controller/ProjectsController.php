<?php

namespace App\Controller;

use Symfony\Component\Routing\Annotation\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Serializer\SerializerInterface;
use App\Entity\Projects;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Form\FormErrorIterator;
use App\Service\FileService;
use App\Repository\ProjectsRepository;

/**
 * @Route("/rest/projects", name="rest_projects")         
 */
class ProjectsController extends Controller
{
    private $serializer;
    
    public function __construct(SerializerInterface $serializer) {
        $this->serializer = $serializer;
    }

    /**
     * @Route("/", methods="GET")
     */
    public function all(ProjectsRepository $repo){

        $list = $repo->findAll();

        $data = $this->serializer->normalize($list, null, ['attributes' => ['id', 'name', 'description', 'image', 'link']]);

        $response = JsonResponse::fromJsonString($this->serializer->serialize($data, "json"));
        return $response;
        // $repo = $this->getDoctrine()->getRepository(Projects::class);
        // $projects = $repo->findAll();
        // $json = $this->serializer->serialize($projects, "json");

        // return JsonResponse::fromJsonString($json);
    }

     /**
     * @Route("/", methods="POST")
     */
    public function add(Request $req, FileService $fileService) {
        //On récupère le fichier dans la request
        $image = $req->files->get("url");
        //On récupère l'url absolue de notre application
        $absoluteUrl = $req->getScheme() . '://' . $req->getHttpHost() . $req->getBasePath();
        //On utilise le fileService pour uploader l'image
        $imageUrl = $fileService->upload($image, $absoluteUrl);

        $projects = new Projects();

        $projects->setName($req->get("name"));       
        $projects->setDescription($req->get("description"));
        $projects->setImage($imageUrl);
        $projects->setLink($req->get("link"));


        $manager = $this->getDoctrine()->getManager();
        $manager->persist($projects);
        $manager->flush();

        $json = $this->serializer->serialize($projects, "json");

        return JsonResponse::fromJsonString($json);
    }
    
     /**
      * @Route("/delete/{projects}", methods="DELETE")
      */
     public function remove(Projects $projects) 
     {
         $manager = $this->getDoctrine()->getManager();
         $manager->remove($projects);
         $manager->flush();
        
        return new Response();
     }


     

    ///////////

//     private $serializer;
//     public function __construct(SerializerInterface $serializer) {
//         $this->serializer = $serializer;
//     }

//     /**
//      * @Route("/",methods={"GET"} )
//      */
//     public function index(SerializerInterface $serializer)
//     {
//        return $response;

//     $projects = new Projects();
//     $projects->setName("test");
//     $projects->setsurname("surtest");
//     $projects->setLevel(5);
//     $projects->setTech(["symfony", "javascript"]);

//         $repo=$this->getDoctrine()->getRepository(Projects::class);

//     $json = $serializer->serialize($repo->findAll(), "json");
//     return JsonResponse::fromJsonString($json);
//     }

//     /**
//      * @Route("/", methods={"POST"})
//      */
//     public function add(Request $request){

//         $form = $this->createForm(ProjectsType::class);

//         $form->submit(json_decode($request->getContent(), true)); 

//         if($form->isSubmitted() && $form-> isValid()){
//             $projects = $form->getData(); 
//         $manager = $this->getDoctrine()->getManager(); 
//         $manager->persist($projects);
//         $manager->flush();
       
//         return new JsonResponse(["id"=>$projects->getId()]);
//         }
//         return new JsonResponse(["errors"=>
//                     $this->listFormErrors($form->getErrors())], 500);
//     }

//     /**
//      * @Route("/{projects}", methods="GET")
//      */
//     public function single(Projects $projects) 
//     {
//         $json = $this->serializer->serialize($projects, "json");
//         return JsonResponse::fromJsonString($json);
//     }

//      /**
//      * @Route("/{projects}", methods="DELETE")
//      */
//     public function remove(Projects $projects) 
//     {
//         $manager = $this->getDoctrine()->getManager();
//         $manager->remove($projects);
//         $manager->flush();
        
//         return new Response();
//     }
//     /**
//      * @Route("/{projects}", methods="PUT")
//      */
//     public function update(Projects $projects, Request $request) 
//     {
//         $body = $request->getContent();
//         $updated = $this->serializer->deserialize($body, Projects::class, "json"); //

//         $manager = $this->getDoctrine()->getManager();
//         $projects->setName($updated->getName());
//         $projects->setSurname($updated->getSurname());

//         $projects->setLevel($updated->getLevel());
//         $projects->setTech($updated->getTech());

       
//         $manager->flush();
        
//         return new Response("", 204);
//     }

//     private function listFormErrors(FormErrorIterator $errors){ //renvoie une liste d'erreurs
// //convertit le formerroriterator en un tableau de strings. 
//         foreach ($errors as $error) {
//             $list[] = $error->getMessage();
//         }
//     }
}

